#!/usr/bin/env bash

# fail if any command fails
set -e
# debug log
set -x

cd ..
git clone -b beta https://github.com/flutter/flutter.git
export PATH=`pwd`/flutter/bin:$PATH

sudo gem uninstall cocoapods -x --all
sudo gem install cocoapods -v 1.7.5
pod setup

flutter config --no-analytics
flutter channel stable
flutter doctor

echo "Installed flutter to `pwd`/flutter"

flutter build ios --release --no-codesign