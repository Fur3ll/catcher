import 'package:flame/flame.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'wolf_game.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  List<DeviceOrientation> list = [
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight
  ];

  await SystemChrome.setEnabledSystemUIOverlays([]);
  await SystemChrome.setPreferredOrientations(list);

  SharedPreferences storage = await SharedPreferences.getInstance();

  await Flame.images.loadAll(<String>[
    'btn/nav-button.png',
    'clock/clock.png',
    'btn/button_1_1.png',
    'btn/button_1_2.png',
    'btn/button_1_3.png',
    'btn/button_1_4.png',
    'menu/game_A.png',
    'menu/game_B.png',
    'menu/menu.png',
    'menu/pause.png',
    'drop/walk.png',
    'drop/ufo.png',
    'hand/hand_left_bottom.png',
    'hand/hand_left_top.png',
    'hand/hand_right_bottom.png',
    'hand/hand_right_top.png',
    'bg/back.png',
    'body/body_right.png',
    'body/body_left.png',
    'light/light.png',
    'light/nonlight.png',
    'penalty/death.png',
    'penalty/heart.png',
  ]);

  WolfGame game = WolfGame(storage);
  runApp(game.widget);
}
