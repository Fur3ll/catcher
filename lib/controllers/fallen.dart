import 'dart:ui';

import '../componets/base/changeable.dart';
import '../componets/base/common/orientations.dart';
import '../componets/runaway.dart';
import '../wolf_game.dart';

///Controller for dropped egg animation
class Fallen {
  final WolfGame game;
  final DropOrient orient;
  final FinishedAnimCallback callback;
  final double speed = .5;
  final double rightDx = .62;
  final double commonDy = 1.3;
  final double leftDx = .5;
  final double gepScale = .2;
  final double leftGepScale = .23;
  final double rightGepScale = .2;

  List<Runaway> runawayList;
  double gep;
  double timer;
  bool isPlaying;
  bool active;
  int step;
  double dx;
  double dy;

  Fallen(this.game, this.orient, this.callback) {
    runawayList = List<Runaway>();
    isPlaying = false;
    active = false;
    step = -1;
    timer = 0;
    dx = 0;
    dy = 0;
    runawayList.add(RunawayFirst(game, this));
    runawayList.add(RunawaySecond(game, this));
    runawayList.add(RunawayThird(game, this));
    runawayList.add(RunawayFour(game, this));
  }

  void update(double t) {
    if (isPlaying) {
      if (speed < timer) {
        timer = 0;
        step += 1;
        if (step == runawayList.length) {
          reset();
          callback();
        } else {
          game.audioCatchFall.play(game.soundFall + game.formatPrefix);
        }
        runawayList.forEach((Runaway itm) => itm.onStep());
      } else {
        timer += t;
      }
    }
  }

  void render(Canvas canvas) {
    if (orient == DropOrient.leftDrop) {
      canvas.save();
      canvas.scale(-1, 1);
    }
    runawayList.forEach((Changeable itm) => itm.render(canvas));
    canvas.restore();
  }

  void resize() {
    if (orient == DropOrient.rightDrop) {
      dx = game.screenWightHalf + (game.tileSize * rightDx);
      dy = game.screenHeightHalf + (game.tileSize * commonDy);
      gep = game.tileSize * rightGepScale;
    } else {
      dx = -(game.screenWightHalf - (game.tileSize * leftDx));
      dy = game.screenHeightHalf + (game.tileSize * commonDy);
      gep = game.tileSize * leftGepScale;
    }
    runawayList.forEach((Changeable itm) => itm.resize());
  }

  void pause() {
    isPlaying = false;
  }

  void reset() {
    isPlaying = false;
    active = false;
    step = -1;
    runawayList.forEach((Runaway itm) => itm.onStep());
  }

  void start() {
    isPlaying = true;
    active = true;
  }
}
