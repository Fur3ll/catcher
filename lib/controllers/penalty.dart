import 'dart:ui';

import 'package:flame/sprite.dart';

import '../wolf_game.dart';
import '../componets/chickens.dart';

///Controller for penalties display
class Penalty {
  final WolfGame game;
  final double speed = .5;
  final int maxPenalty = 6;
  final positionDx;
  final positionDy;
  final Sprite penaltySpriteOn = Sprite('penalty/death.png');
  final Sprite penaltySpriteOff = Sprite('penalty/heart.png');
  List<Chickens> penaltyList;
  double timer;
  bool isPlaying = true;
  double gep;
  int step;
  int penalty;
  double dx;
  double dy;

  Penalty(this.game, this.positionDx, this.positionDy) {
    penaltyList = List<Chickens>();
    step = -1;
    timer = 0;
    penalty = 0;
    gep = 0;
    dx = 0;
    dy = 0;
    penaltyList
        .add(ChickenFirst(game, penaltySpriteOn, penaltySpriteOff, this));
    penaltyList
        .add(ChickenSecond(game, penaltySpriteOn, penaltySpriteOff, this));
    penaltyList
        .add(ChickenThird(game, penaltySpriteOn, penaltySpriteOff, this));
  }

  void update(double t) {
    if (isPlaying) {
      if (speed < timer) {
        timer = 0;
        penaltyList.forEach((Chickens itm) => itm.onStep());
      } else {
        timer += t;
      }
    } else {
      penaltyList.forEach((Chickens itm) => itm.setVisible());
    }
  }

  void render(Canvas canvas) {
    penaltyList.forEach((Chickens itm) => itm.render(canvas));
  }

  void resize() {
    dx = (game.screenSize.width / 2) + (game.tileSize * positionDx);
    dy = (game.screenSize.height / 2) - (game.tileSize * positionDy);
    gep = game.tileSize * 0.4;
    penaltyList.forEach((Chickens itm) => itm?.resize());
  }

  void setPenalty() {
    penalty += 2;
    checkLoos();
  }

  void checkLoos() {
    if (penalty >= maxPenalty) {
      isPlaying = false;
    }
  }

  void setHalfPenalty() {
    penalty += 1;
    checkLoos();
  }

  void reset() {
    penalty = 0;
    penaltyList.forEach((Chickens itm) => itm.isChanged = true);
    isPlaying = true;
  }
}
