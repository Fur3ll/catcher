import 'dart:ui';
import 'package:flame/sprite.dart';

import '../wolf_game.dart';
import '../componets/eggs.dart';
import '../componets/base/common/orientations.dart';

///Controller for egg animation
class Farm {
  final WolfGame game;
  final Orient orient;
  final Sprite eggSpriteOn = Sprite('light/light.png');
  final Sprite eggSpriteOff = Sprite('light/nonlight.png');
  final double topDy = .3;
  final double botDy = .4;
  final double leftDx = 1.3;
  final double rightDx = 1.4;
  final double gepScale = .2;

  List<Eggs> eggsList;
  double gep;
  List<int> stepList;
  double dx;
  double dy;

  Farm(this.game, this.orient) {
    eggsList = List<Eggs>();
    stepList = [-1, -1];
    dx = 0;
    dy = 0;
    eggsList.add(EggsFirst(game, eggSpriteOn, eggSpriteOff, this));
    eggsList.add(EggsSecond(game, eggSpriteOn, eggSpriteOff, this));
    eggsList.add(EggsThird(game, eggSpriteOn, eggSpriteOff, this));
    eggsList.add(EggsFour(game, eggSpriteOn, eggSpriteOff, this));
    eggsList.add(EggsFive(game, eggSpriteOn, eggSpriteOff, this));
  }

  void render(Canvas canvas) {
    if (orient == Orient.leftTop || orient == Orient.leftBot) {
      canvas.save();
      canvas.scale(-1, 1);
    }
    eggsList.forEach((Eggs itm) => itm.render(canvas));
    canvas.restore();
  }

  void resize() {
    switch (orient) {
      case Orient.leftBot:
        dx = -(game.screenWightHalf - (game.tileSize * leftDx));
        dy = (game.screenHeightHalf + (game.tileSize * botDy));
        break;
      case Orient.leftTop:
        dx = -((game.screenWightHalf - (game.tileSize * leftDx)));
        dy = (game.screenHeightHalf - (game.tileSize * topDy));
        break;
      case Orient.rightBot:
        dx = game.screenWightHalf + (game.tileSize * rightDx);
        dy = (game.screenHeightHalf + (game.tileSize * botDy));
        break;
      case Orient.rightTop:
        dx = game.screenWightHalf + (game.tileSize * rightDx);
        dy = game.screenHeightHalf - (game.tileSize * topDy);
        break;
    }
    gep = game.tileSize * gepScale;
    eggsList.forEach((Eggs itm) => itm.resize());
  }
}
