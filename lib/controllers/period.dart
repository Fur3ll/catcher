import 'dart:math';
import 'dart:ui';

import 'package:catch_the_egg/componets/clocks.dart';
import 'package:catch_the_egg/wolf_game.dart';

///Controller for clock appearance
class Period {
  final WolfGame game;
  final int maxInterval = 15000;
  final int minInterval = 10000;
  final int maxPause = 55000;
  final int minPause = 25000;
  final int changePeriod = 70;

  Random random;
  int nextPeriod;
  int nextInterval;
  int intervalRestore;
  List<Clock> clockList;

  bool isPlaying = true;
  bool isPeriod = false;
  bool playing = false;
  bool needChange = false;

  Period(this.game) {
    clockList = List<Clock>();
    clockList.add(ClockFirst(game));
    clockList.add(ClockSecond(game));
    nextPeriod = 0;
    nextInterval = DateTime.now().millisecondsSinceEpoch + maxPause;
    random = Random();
  }

  void render(Canvas canvas) {
    clockList.forEach((Clock itm) => itm.render(canvas));
  }

  void resize() {
    clockList.forEach((Clock itm) => itm.resize());
  }

  void stop() {
    isPlaying = false;
  }

  void pause() {
    stop();
    int nowTimestamp = DateTime.now().millisecondsSinceEpoch;
    intervalRestore = nextInterval - nowTimestamp;
  }

  void restart() {
    int nowTimestamp = DateTime.now().millisecondsSinceEpoch;
    nextInterval = nowTimestamp + intervalRestore;
    if(nextInterval < nowTimestamp) {
      start();
    } else {
      isPlaying = true;
    }
  }

  void start() {
    int nowTimestamp = DateTime.now().millisecondsSinceEpoch;
    nextInterval = nowTimestamp +
        (minPause + random.nextInt(maxPause - minPause));
    nextPeriod = 0;
    clockList.forEach((Clock itm) => itm.setGone());
    playing = false;
    isPlaying = true;
  }

  void blink() {
    nextPeriod += 1;
    if (nextPeriod >= changePeriod) {
      if (playing) {
        isPeriod = true;
        if (needChange) {
          clockList.forEach((Clock itm) {
            if (clockList.indexOf(itm).isOdd) {
              itm.setVisible();
            } else {
              itm.setGone();
            }
          });
          needChange = false;
        } else {
          clockList.forEach((Clock itm) {
            if (clockList.indexOf(itm).isEven) {
              itm.setVisible();
            } else {
              itm.setGone();
            }
          });
          needChange = true;
        }
        nextPeriod = 0;
      } else {
        isPeriod = false;
        clockList.forEach((Clock itm) => itm.setGone());
      }
    }
  }

  void update(double t) {
    if (isPlaying) {
      int nowTimestamp = DateTime.now().millisecondsSinceEpoch;
      if (nowTimestamp >= nextInterval) {
        if (playing) {
          playing = false;
          nextInterval = nowTimestamp +
              (minPause + random.nextInt(maxPause - minPause));
        } else {
          playing = true;
          nextInterval = nowTimestamp +
              (minInterval + random.nextInt(maxInterval - minInterval));
        }
      }
      blink();
    }
  }
}
