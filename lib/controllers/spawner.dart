import 'dart:math';

import 'package:catch_the_egg/componets/base/common/game_state.dart';

import '../componets/eggs.dart';
import '../wolf_game.dart';
import 'farm.dart';

///Controller for egg spawn
class EggSpawner {
  final WolfGame game;
  final int defaultDifficulty = -1;

  double get speed => game.speed;

  GameState get gameState => game.gameState;

  int get maxEggsOnScreen => game.maxEggsOnScreen;

  List<Farm> farmList;
  double timer;
  Random rnd;
  int maxFarm;
  int currentEggsOnScreen;
  bool lost;
  bool playing;
  int pastFarmSpawn;
  int excludedFarm;
  int currentFarmSpawn;
  int startingStep;

  final DroppedCallback callback;
  final CatchCallback catchCallback;

  EggSpawner(this.game, this.farmList, this.callback, this.catchCallback) {
    rnd = Random();
    timer = 0;
    excludedFarm = -1;
    maxFarm = farmList.length;
    pastFarmSpawn = 0;
    currentEggsOnScreen = 0;
    currentFarmSpawn = 0;
    lost = false;
    playing = false;
    startingStep = _getRandom();
    start();
  }

  void update(double t) {
    if (playing) {
      if (speed < timer) {
        timer = 0;
        stepLogic();
      } else {
        timer += t;
      }
    }
  }

  void stepLogic() {
    _resetStep();
    Farm farm = farmList[startingStep];
    bool eggSpawned = false;
    bool audioPlayed = false;

    farm.stepList.forEach((int step) {
      if (step > -1) {
        farm.stepList[farm.stepList.indexOf(step)] += 1;

        if (!audioPlayed && step < farm.eggsList.length - 1) {
          _playAudio(farm);
          audioPlayed = true;
        }
      } else if (currentFarmSpawn == farmList.indexOf(farm) &&
          !eggSpawned &&
          maxEggsOnScreen > currentEggsOnScreen) {
        farm.stepList[farm.stepList.indexOf(step)] += 1;
        eggSpawned = true;
        currentEggsOnScreen += 1;
        pastFarmSpawn = farmList.indexOf(farm);

        if (!audioPlayed) {
          _playAudio(farm);
          audioPlayed = true;
        }
      }
    });
    _checkForCatch(farm);
    farm.eggsList.forEach((Eggs itm) => itm.onStep());
    startingStep += 1;
  }

  void _playAudio(Farm farm) {
    switch (farmList.indexOf(farm)) {
      case 0:
        game.audioFarm1.play(game.soundFarm1 + game.formatPrefix);
        break;
      case 1:
        game.audioFarm2.play(game.soundFarm2 + game.formatPrefix);
        break;
      case 2:
        game.audioFarm3.play(game.soundFarm3 + game.formatPrefix);
        break;
      case 3:
        game.audioFarm4.play(game.soundFarm4 + game.formatPrefix);
        break;
      default:
        break;
    }
  }

  void _checkForCatch(Farm farm) {
    farm.stepList.forEach((int step) {
      if (step == farm.eggsList.length) {
        if (game.orient == farm.orient) {
          game.audioCatchFall.play(game.soundCatch + game.formatPrefix);
          catchCallback();
        } else {
          callback(farm.orient);
        }
        currentEggsOnScreen -= 1;
        farm.stepList[farm.stepList.indexOf(step)] = -1;
      }
    });
  }

  void stop() {
    playing = false;
  }

  void start() {
    playing = true;
  }

  void clear() {
    farmList.forEach((Farm farm) {
      farm.stepList
          .forEach((int itm) => farm.stepList[farm.stepList.indexOf(itm)] = -1);
      farm.eggsList.forEach((Eggs itm) => itm.onStep());
    });
    currentEggsOnScreen = 0;
  }

  void difficultyChange(int penalty) {
    switch (penalty) {
      case 0:
        excludedFarm = 2;
        break;
      case 1:
        excludedFarm = 3;
        break;
      case 2:
        excludedFarm = 3;
        break;
      case 3:
        excludedFarm = 0;
        break;
      case 4:
        excludedFarm = 0;
        break;
      case 5:
        excludedFarm = 1;
        break;
      default:
        excludedFarm = -1;
        break;
    }
  }

  void _resetStep() {
    if (startingStep > farmList.length - 1) {
      startingStep = 0;
      currentFarmSpawn = _getRandom();
      if (maxEggsOnScreen == currentEggsOnScreen) {
        currentFarmSpawn = pastFarmSpawn;
      }
    }
  }

  int _getRandom() {
    int randomNumber;
    while (true) {
      randomNumber = rnd.nextInt(maxFarm);
      if (randomNumber != pastFarmSpawn && randomNumber != excludedFarm) {
        return randomNumber;
      }
    }
  }
}
