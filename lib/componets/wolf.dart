import 'dart:ui';

import 'package:flame/sprite.dart';

import '../wolf_game.dart';
import 'base/tapable.dart';
import 'base/common/orientations.dart';

class Wolf extends TapAble {
  final DropOrient orient;
  final double dxRight = .226;
  final double dxLeft = .36;
  final double rectSizeRightX = .7;
  final double rectSizeLeftX = .48;
  final double rectSizeY = 1.2;

  Wolf(WolfGame game, this.orient) : super(game) {
    switch (orient) {
      case DropOrient.leftDrop:
        sprite = Sprite('body/body_left.png');
        break;
      case DropOrient.rightDrop:
        sprite = Sprite('body/body_right.png');
        break;
    }
  }

  void resize() {
    switch (orient) {
      case DropOrient.leftDrop:
        rect = Rect.fromLTWH(
          game.screenWightHalf - (game.tileSize * dxRight),
          game.screenHeightHalf,
          game.tileSize * rectSizeLeftX,
          game.tileSize * rectSizeY,
        );
        break;
      case DropOrient.rightDrop:
        rect = Rect.fromLTWH(
          game.screenWightHalf - (game.tileSize * dxLeft),
          game.screenHeightHalf,
          game.tileSize * rectSizeRightX,
          game.tileSize * rectSizeY,
        );
        break;
    }
  }

  void onTapDown() {
    if (game.orient != Orient.leftTop && game.orient != Orient.leftBot) {
      switch (orient) {
        case DropOrient.leftDrop:
          setGone();
          break;
        case DropOrient.rightDrop:
          setVisible();
          break;
      }
    } else {
      switch (orient) {
        case DropOrient.leftDrop:
          setVisible();
          break;
        case DropOrient.rightDrop:
          setGone();
          break;
      }
    }
  }
}
