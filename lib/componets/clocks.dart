import 'dart:ui';

import 'package:catch_the_egg/componets/base/changeable.dart';
import 'package:catch_the_egg/wolf_game.dart';
import 'package:flame/sprite.dart';

abstract class Clock extends Changeable {
  final double scaleDx = .3;
  final double scaleDy = .3;
  final double commonDx = 1.2;
  final double dyTop = 1.5;
  final double dyBot = 1.1;

  Clock(WolfGame game) : super(game) {
    sprite = Sprite('clock/clock.png');
  }
}

class ClockFirst extends Clock {
  ClockFirst(WolfGame game) : super(game);

  @override
  void resize() {
    rect = Rect.fromLTWH(
      game.screenWightHalf - (game.tileSize * commonDx),
      game.screenHeightHalf - (game.tileSize * dyTop),
      game.tileSize * scaleDx,
      game.tileSize * scaleDy,
    );
  }
}

class ClockSecond extends Clock {
  ClockSecond(WolfGame game) : super(game);

  @override
  void resize() {
    rect = Rect.fromLTWH(
      game.screenWightHalf - (game.tileSize * commonDx),
      game.screenHeightHalf - (game.tileSize * dyBot),
      game.tileSize * scaleDx,
      game.tileSize * scaleDy,
    );
  }
}