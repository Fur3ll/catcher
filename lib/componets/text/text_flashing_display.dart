import 'dart:math';

import 'package:catch_the_egg/componets/base/common/game_state.dart';
import 'package:catch_the_egg/componets/text/text_display.dart';
import 'package:catch_the_egg/wolf_game.dart';

class FlashingText extends TextDisplay {

  final double speed = .5;
  GameState get state => game.gameState;
  double timer = 0;
  bool isChanged = true;

  FlashingText(WolfGame game, positionDx, positionDy, fontSize) : super(game, positionDx, positionDy, fontSize);

  void update(double t) {
    if (state == GameState.pause) {
      if (speed < timer) {
        timer = 0;
        if(isChanged) {
          setVisible();
          isChanged = false;
        } else {
          setGone();
          isChanged = true;
        }
      } else {
        timer += t;
      }
    } else {
      timer = 0;
    }
  }
}