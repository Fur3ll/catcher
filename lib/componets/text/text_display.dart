import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart' as prefix0;
import 'package:flutter/material.dart';

import '../../wolf_game.dart';

class TextDisplay {
  final WolfGame game;
  final positionDx;
  final positionDy;
  final fontSize;
  Rect rect;
  TextPainter tp;
  Offset textOffset;
  bool isVisible;

  TextDisplay(this.game, this.positionDx, this.positionDy, this.fontSize, ) {
    tp = TextPainter(
      textAlign: TextAlign.center,
      textDirection: TextDirection.ltr,
    );
    isVisible = true;
  }

  void render(Canvas canvas) {
    if(isVisible) {
      tp.paint(canvas, textOffset);
    }
  }

  void resize(String text) {
    rect = Rect.fromLTWH(
      game.screenWightHalf - (game.tileSize * positionDx),
      game.screenHeightHalf - (game.tileSize * positionDy),
      game.tileSize,
      game.tileSize,
    );
    tp.text = TextSpan(
      text: text,
      style: prefix0.TextStyle(
        color: Colors.black,
        fontSize: game.tileSize * fontSize,
      ),
    );
    tp.layout();
    textOffset = Offset(
      rect.center.dx - (tp.width / 2),
      rect.top + (rect.height * .4) - (tp.height / 2),
    );
  }

  void updateText(String s) {
    tp.text = TextSpan(
      text: s,
      style: prefix0.TextStyle(
        color: Colors.black,
        fontSize: game.tileSize * fontSize,
      ),
    );
    tp.layout();
    textOffset = Offset(
      rect.center.dx - (tp.width / 2),
      rect.top + (rect.height * .4) - (tp.height / 2),
    );
  }

  void setVisible() {
    isVisible = true;
  }

   void setGone() {
    isVisible = false;
   }
}
