import 'dart:ui';

import 'package:flame/sprite.dart';

import '../../wolf_game.dart';

abstract class Switchable {
  final WolfGame game;
  final Sprite spriteOn;
  final Sprite spriteOff;
  bool isVisible = false;
  Rect rect;
  final Paint visiblePaint = Paint()..color = Color(0xFFFFFFFF).withAlpha(255);
  final Paint halfPaint = Paint()..color = Color(0xFFFFFFFF).withAlpha(20);
  final Paint gonePaint = Paint()..color = Color(0xFFFFFFFF).withAlpha(0);

  Switchable(this.game, this.spriteOn, this.spriteOff);

  void render(Canvas c);

  void setVisible() {
    isVisible = true;
  }

  void setGone() {
    isVisible = false;
  }

  void resize();
}
