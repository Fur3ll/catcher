import 'dart:ui';
import 'package:flame/sprite.dart';

import '../../wolf_game.dart';

abstract class Changeable {
  final WolfGame game;
  Sprite sprite;
  Rect rect;
  bool isVisible = false;
  final Paint visiblePaint = Paint()..color = Color(0xFFFFFFFF).withAlpha(255);
  final Paint gonePaint = Paint()..color = Color(0xFFFFFFFF).withAlpha(0);

  Changeable(this.game);

  void render(Canvas c) {
    if (isVisible) {
      sprite.renderRect(c, rect, overridePaint: visiblePaint);
    } else {
      sprite.renderRect(c, rect, overridePaint: gonePaint);
    }
  }

  void setVisible() {
    isVisible = true;
  }

  void setGone() {
    isVisible = false;
  }

  void resize();
}
