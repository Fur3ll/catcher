import 'dart:ui';

import 'package:catch_the_egg/wolf_game.dart';

abstract class BaseComponent {
  final WolfGame game;

  BaseComponent(this.game);

  void render(Canvas canvas);

  void resize();
}
