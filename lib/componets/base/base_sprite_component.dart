import 'dart:ui';

import 'package:catch_the_egg/componets/base/base_component.dart';
import 'package:catch_the_egg/wolf_game.dart';
import 'package:flame/sprite.dart';

abstract class BaseSpriteComponent extends BaseComponent {
  final Sprite sprite;
  Rect rect;

  BaseSpriteComponent(WolfGame game, this.sprite) : super(game);

  void render(Canvas canvas) {
      sprite.renderRect(canvas, rect);
  }

  void resize();
}
