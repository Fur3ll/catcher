import 'dart:ui';

import 'package:catch_the_egg/componets/base/base_component.dart';
import 'package:catch_the_egg/wolf_game.dart';
import 'package:flame/position.dart';
import 'package:flame/svg.dart';

abstract class BaseVectorComponent extends BaseComponent {
  final Svg svg;
  Rect rect;

  BaseVectorComponent(WolfGame game, this.svg) : super(game);

  void render(Canvas canvas) {
    if (svg.loaded()) {
      svg.renderPosition(
          canvas, Position(rect.left, rect.top), rect.width, rect.height);
    }
  }

  void resize();
}