import '../../wolf_game.dart';
import 'changeable.dart';

abstract class TapAble extends Changeable {
  TapAble(WolfGame game) : super(game) {
    onTapDown();
  }

  void onTapDown();
}
