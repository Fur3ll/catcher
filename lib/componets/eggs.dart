import 'dart:ui';
import 'package:catch_the_egg/componets/base/switchable.dart';
import 'package:flame/sprite.dart';

import '../wolf_game.dart';
import '../controllers/farm.dart';

abstract class Eggs extends Switchable {
  final Farm farm;
  final double rectSizeX = .15;
  final double rectSizeY = .18;
  double get gep => farm.gep;

  Eggs(WolfGame game, Sprite spriteOn, Sprite spriteOff, this.farm) : super(game, spriteOn, spriteOff);

  void onStep() {
    farm.stepList.forEach((int step) {
      if (step == farm.eggsList.indexOf(this)) {
        setVisible();
      } else if (farm.stepList.indexOf(step) > 0) {
        if (farm.stepList[farm.stepList.indexOf(step) -1] == farm.eggsList.indexOf(this)) {
          setVisible();
        } else if (step == farm.eggsList.indexOf(this)) {
          setVisible();
        } else {
          setGone();
        }
      } else {
        setGone();
      }
    });
  }

  void render(Canvas c) {
      if (isVisible) {
        spriteOn.renderRect(c, rect,
            overridePaint: visiblePaint);
      } else {
        spriteOff.renderRect(c, rect,
            overridePaint: halfPaint);
      }
  }

  void resize();
}

class EggsFirst extends Eggs {
  EggsFirst(WolfGame game, Sprite spriteOn, Sprite spriteOff, Farm farm) : super(game, spriteOn, spriteOff, farm);

  void resize() {
    rect = Rect.fromLTWH(
      farm.dx,
      farm.dy + gep * .6,
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}

class EggsSecond extends Eggs {
  EggsSecond(WolfGame game, Sprite spriteOn, Sprite spriteOff, Farm farm) : super(game, spriteOn, spriteOff, farm);

  void resize() {
    rect = Rect.fromLTWH(
      farm.dx - gep,
      farm.dy + gep * .8,
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}

class EggsThird extends Eggs {
  EggsThird(WolfGame game, Sprite spriteOn, Sprite spriteOff, Farm farm) : super(game, spriteOn, spriteOff, farm);

  void resize() {
    rect = Rect.fromLTWH(
      farm.dx - gep * 2,
      farm.dy + gep,
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}

class EggsFour extends Eggs {
  EggsFour(WolfGame game, Sprite spriteOn, Sprite spriteOff, Farm farm) : super(game, spriteOn, spriteOff, farm);

  void resize() {
    rect = Rect.fromLTWH(
      farm.dx - gep * 3,
      farm.dy + gep * 1.2,
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}

class EggsFive extends Eggs {
  EggsFive(WolfGame game, Sprite spriteOn, Sprite spriteOff, Farm farm) : super(game, spriteOn, spriteOff, farm);

  void resize() {
    rect = Rect.fromLTWH(
      farm.dx - gep * 4,
      farm.dy + gep * 1.4,
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}
