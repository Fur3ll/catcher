import 'dart:ui';
import 'package:flame/sprite.dart';

import '../wolf_game.dart';
import 'base/tapable.dart';
import 'base/common/orientations.dart';

class Basket extends TapAble {
  final Orient orient;
  final double dxLeftBasket = .52;
  final double dxRightBasket = 18;
  final double dyBotBasket = 2.7;
  final double dyTopBasket = 15;

  Basket(WolfGame game, this.orient) : super(game) {
    switch (orient) {
      case Orient.leftBot:
        sprite = Sprite('hand/hand_left_bottom.png');
        break;
      case Orient.leftTop:
        sprite = Sprite('hand/hand_left_top.png');
        break;
      case Orient.rightBot:
        sprite = Sprite('hand/hand_right_bottom.png');
        break;
      case Orient.rightTop:
        sprite = Sprite('hand/hand_right_top.png');
        break;
    }
  }

  @override
  void onTapDown() {
    if (game.orient == orient) {
      setVisible();
    } else {
      setGone();
    }
  }

  @override
  void resize() {
    switch (orient) {
      case Orient.leftBot:
        rect = Rect.fromLTWH(
          game.screenWightHalf - (game.tileSize * dxLeftBasket),
          game.screenHeightHalf + (game.tileSize / dyBotBasket),
          game.tileSize * .7,
          game.tileSize * .7,
        );
        break;
      case Orient.leftTop:
        rect = Rect.fromLTWH(
          game.screenWightHalf - (game.tileSize * dxLeftBasket),
          game.screenHeightHalf + (game.tileSize / dyTopBasket),
          game.tileSize * .7,
          game.tileSize * .5,
        );
        break;
      case Orient.rightBot:
        rect = Rect.fromLTWH(
          game.screenWightHalf - (game.tileSize / dxRightBasket),
          game.screenHeightHalf + (game.tileSize / dyBotBasket),
          game.tileSize * .7,
          game.tileSize * .7,
        );
        break;
      case Orient.rightTop:
        rect = Rect.fromLTWH(
          game.screenWightHalf - (game.tileSize / dxRightBasket),
          game.screenHeightHalf + (game.tileSize / dyTopBasket),
          game.tileSize * .7,
          game.tileSize * .5,
        );
        break;
    }
  }
}
