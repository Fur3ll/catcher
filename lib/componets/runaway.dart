import 'dart:ui';

import 'package:flame/sprite.dart';
import '../controllers/fallen.dart';

import '../wolf_game.dart';
import 'base/changeable.dart';

///Dropped egg animation base class
abstract class Runaway extends Changeable {
  final Fallen fallen;
  final double rectSizeX = .15;
  final double rectSizeY = .18;
  double get gep => fallen.gep;

  Runaway(WolfGame game, this.fallen) : super(game) {
    sprite = Sprite('light/light.png');
  }

  void onStep() {
    if (fallen.step == fallen.runawayList.indexOf(this)) {
      setVisible();
    } else {
      setGone();
    }
  }

  void resize();
}

class RunawayFirst extends Runaway {
  RunawayFirst(WolfGame game, Fallen fallen) : super(game, fallen);

  void resize() {
    rect = Rect.fromLTWH(
      fallen.dx,
      fallen.dy,
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}

class RunawaySecond extends Runaway{
  RunawaySecond(WolfGame game, Fallen fallen) : super(game, fallen);

  void resize() {
    rect = Rect.fromLTWH(
      fallen.dx + gep * 2.3,
      fallen.dy,
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}

class RunawayThird extends Runaway{
  RunawayThird(WolfGame game, Fallen fallen) : super(game, fallen);

  void resize() {
    rect = Rect.fromLTWH(
      fallen.dx + gep * 4.7,
      fallen.dy,
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}

class RunawayFour extends Runaway{
  RunawayFour(WolfGame game, Fallen fallen) : super(game, fallen);

  void resize() {
    rect = Rect.fromLTWH(
      fallen.dx + gep * 7.1,
      fallen.dy,
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}