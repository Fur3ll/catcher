import 'dart:ui';

import 'package:flame/sprite.dart';

import '../controllers/penalty.dart';
import '../wolf_game.dart';
import 'base/switchable.dart';

///Penalties display base class
abstract class Chickens extends Switchable {
  final Penalty penalty;
  final double scaleDx = .25;
  final double scaleDy = .25;
  final double placeDy = .56;
  bool isChanged = true;

  double get gep => penalty.gep;

  Chickens(WolfGame game, Sprite spriteOn, Sprite spriteOff, this.penalty)
      : super(game, spriteOn, spriteOff);

  void render(Canvas c) {
    if (isVisible) {
      spriteOn.renderRect(c, rect, overridePaint: visiblePaint);
    } else {
      if (isChanged) {
        spriteOff.renderRect(c, rect, overridePaint: visiblePaint);
      } else {
        spriteOff.renderRect(c, rect, overridePaint: gonePaint);
      }
    }
  }

  void onStep() {
    if (penalty.penalty.isEven) {
      if (penalty.penaltyList.indexOf(this) <
          (penalty.penalty / penalty.penaltyList.length).ceil()) {
        setVisible();
      } else {
        setGone();
      }
    } else {

      if (penalty.penaltyList.indexOf(this) ==
          (penalty.penalty / penalty.penaltyList.length).round()) {
        if (isChanged) {
          setGone();
          isChanged = false;
        } else {
          setGone();
          isChanged = true;
        }
      } else if (penalty.penaltyList.indexOf(this) <
          (penalty.penalty / penalty.penaltyList.length).ceil()) {
        setVisible();
      } else {
        setGone();
      }
    }
  }
}

class ChickenFirst extends Chickens {
  ChickenFirst(
      WolfGame game, Sprite spriteOn, Sprite spriteOff, Penalty penalty)
      : super(game, spriteOn, spriteOff, penalty);

  void resize() {
    rect = Rect.fromLTWH(
      penalty.dx,
      penalty.dy,
      game.tileSize * scaleDx,
      game.tileSize * scaleDy,
    );
  }
}

class ChickenSecond extends Chickens {
  ChickenSecond(
      WolfGame game, Sprite spriteOn, Sprite spriteOff, Penalty penalty)
      : super(game, spriteOn, spriteOff, penalty);

  void resize() {
    rect = Rect.fromLTWH(
      penalty.dx - gep,
      penalty.dy,
      game.tileSize * scaleDx,
      game.tileSize * scaleDy,
    );
  }
}

class ChickenThird extends Chickens {
  ChickenThird(
      WolfGame game, Sprite spriteOn, Sprite spriteOff, Penalty penalty)
      : super(game, spriteOn, spriteOff, penalty);

  void resize() {
    rect = Rect.fromLTWH(
      penalty.dx - gep * 2,
      penalty.dy,
      game.tileSize * scaleDx,
      game.tileSize * scaleDy,
    );
  }
}
