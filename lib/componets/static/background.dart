import 'dart:ui';
import 'package:catch_the_egg/componets/base/base_sprite_component.dart';
import 'package:flame/sprite.dart';
import '../../wolf_game.dart';

class Background extends BaseSpriteComponent {
  Background(WolfGame game, Sprite sprite) : super(game, sprite);

  @override
  void resize() {
    rect = Rect.fromLTWH(
      0,
      0,
      game.screenSize.width,
      game.screenSize.height,
    );
  }
}
