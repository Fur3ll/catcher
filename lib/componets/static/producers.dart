import 'dart:ui';

import 'package:catch_the_egg/componets/base/base_sprite_component.dart';
import 'package:catch_the_egg/componets/base/common/orientations.dart';
import 'package:catch_the_egg/wolf_game.dart';
import 'package:flame/sprite.dart';

class Producers extends BaseSpriteComponent {
  final Orient orient;
  final double rectTopSizeX = .7;
  final double rectTopSizeY = .7;
  final double rectBotSizeX = .3;
  final double rectBotSizeY = .5;
  final double dxBotLeft = 2.1;
  final double dxTopLeft = 2.3;
  final double dxBotRight = 1.5;
  final double dxTopRight = 1.8;
  final int dyCommon = 2;

  Producers(WolfGame game, Sprite sprite, this.orient) : super(game, sprite);

  @override
  void resize() {
    switch (orient) {
      case Orient.leftBot:
        rect = Rect.fromLTWH(
          game.screenWightHalf - (game.tileSize * dxBotLeft),
          game.screenHeightHalf + (game.tileSize / dyCommon),
          game.tileSize * rectBotSizeX,
          game.tileSize * rectBotSizeY,
        );
        break;
      case Orient.leftTop:
        rect = Rect.fromLTWH(
          game.screenWightHalf - (game.tileSize * dxTopLeft),
          game.screenHeightHalf - (game.tileSize / dyCommon),
          game.tileSize * rectTopSizeX,
          game.tileSize * rectTopSizeY,
        );
        break;
      case Orient.rightBot:
        rect = Rect.fromLTWH(
          game.screenWightHalf + (game.tileSize * dxBotRight),
          game.screenHeightHalf + (game.tileSize / dyCommon),
          game.tileSize * rectTopSizeX,
          game.tileSize * rectTopSizeY,
        );
        break;
      case Orient.rightTop:
        rect = Rect.fromLTWH(
          game.screenWightHalf + (game.tileSize * dxTopRight),
          game.screenHeightHalf - (game.tileSize / dyCommon),
          game.tileSize * rectBotSizeX,
          game.tileSize * rectBotSizeY,
        );
        break;
    }
  }
}
