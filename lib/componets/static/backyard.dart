import 'dart:ui';
import 'package:catch_the_egg/componets/base/base_vector_component.dart';
import 'package:flame/svg.dart';
import '../../wolf_game.dart';

class Backyard extends BaseVectorComponent {
  Backyard(WolfGame game, Svg svg) : super(game, svg);

  @override
  void resize() {
    rect = Rect.fromLTWH(
      (game.screenSize.width - (game.tileSize * 7)) / 2,
      (game.screenSize.height - (game.tileSize * 4.5)) / 2,
      game.tileSize * 7,
      game.tileSize * 4.5,
    );
  }
}
