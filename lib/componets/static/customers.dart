import 'dart:ui';

import 'package:catch_the_egg/componets/base/base_sprite_component.dart';
import 'package:catch_the_egg/wolf_game.dart';
import 'package:flame/sprite.dart';

class Customers extends BaseSpriteComponent {
  final double rectSizeX = .8;
  final double rectSizeY = .7;
  final double dx = 2.3;
  final double dy = 1.5;

  Customers(WolfGame game, Sprite sprite) : super(game, sprite);

  @override
  void resize() {
    rect = Rect.fromLTWH(
      game.screenWightHalf - (game.tileSize * dx),
      game.screenHeightHalf - (game.tileSize * dy),
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}
