import 'dart:ui';

import 'package:flame/sprite.dart';
import '../wolf_game.dart';
import 'base/common/orientations.dart';

abstract class Button {
  final WolfGame game;
  Sprite bgSprite;
  Rect rect;

  Button(this.game);

  void render(Canvas canvas) {
    bgSprite.renderRect(canvas, rect);
  }

  void resize();
}

abstract class ControlButton extends Button {
  final double rectSizeX = 1.1;
  final double rectSizeY = .8;
  final double dxRight = 1.22;
  final double dxLeft = .15;
  final double dyTop = 1;
  final double dyBot = 2.2;
  Orient orient;

  ControlButton(WolfGame game) : super(game);
}

class TopLeftButton extends ControlButton {
  TopLeftButton(WolfGame game) : super(game) {
    bgSprite = Sprite('btn/button_1_1.png');
    orient = Orient.leftTop;
  }

  @override
  void resize() {
    rect = Rect.fromLTWH(
      game.tileSize * dxLeft,
      game.screenSize.height - (game.tileSize * dyBot),
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}

class BotLeftButton extends ControlButton {
  BotLeftButton(WolfGame game) : super(game) {
    bgSprite = Sprite('btn/button_1_2.png');
    orient = Orient.leftBot;
  }

  @override
  void resize() {
    rect = Rect.fromLTWH(
      game.tileSize * dxLeft,
      game.screenSize.height - (game.tileSize * dyTop),
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}

class TopRightButton extends ControlButton {
  TopRightButton(WolfGame game) : super(game) {
    bgSprite = Sprite('btn/button_1_3.png');
    orient = Orient.rightTop;
  }

  @override
  void resize() {
    rect = Rect.fromLTWH(
      game.screenSize.width - (game.tileSize * dxRight),
      game.screenSize.height - (game.tileSize * dyBot),
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}

class BotRightButton extends ControlButton {
  BotRightButton(WolfGame game) : super(game) {
    bgSprite = Sprite('btn/button_1_4.png');
    orient = Orient.rightBot;
  }

  @override
  void resize() {
    rect = Rect.fromLTWH(
      game.screenSize.width - (game.tileSize * dxRight),
      game.screenSize.height - (game.tileSize * dyTop),
      game.tileSize * rectSizeX,
      game.tileSize * rectSizeY,
    );
  }
}

abstract class ModeButton extends Button {
  final double btnRectSizeX = .6;
  final double btnRectSizeY = .5;
  final double commonDx = 3.5;
  final double btnADy = 2.2;
  final double btnBDy = 1.6;

  ModeButton(WolfGame game) : super(game);
}

class ModeButtonA extends ModeButton {
  ModeButtonA(WolfGame game) : super(game) {
    bgSprite = Sprite('menu/game_A.png');
  }

  @override
  void resize() {
    rect = Rect.fromLTWH(
      (game.screenSize.width / 2) + (game.tileSize * commonDx),
      (game.screenSize.height / 2) - (game.tileSize * btnADy),
      game.tileSize * btnRectSizeX,
      game.tileSize * btnRectSizeY,
    );
  }
}

class ModeButtonB extends ModeButton {
  ModeButtonB(WolfGame game) : super(game) {
    bgSprite = Sprite('menu/game_B.png');
  }

  @override
  void resize() {
    rect = Rect.fromLTWH(
      (game.screenSize.width / 2) + (game.tileSize * commonDx),
      (game.screenSize.height / 2) - (game.tileSize * btnBDy),
      game.tileSize * btnRectSizeX,
      game.tileSize * btnRectSizeY,
    );
  }
}

class MenuButton extends ModeButton {
  MenuButton(WolfGame game) : super(game) {
    bgSprite = Sprite('menu/pause.png');
  }

  @override
  void resize() {
    rect = Rect.fromLTWH(
      (game.screenSize.width / 2) + (game.tileSize * commonDx),
      (game.screenSize.height / 2) - (game.tileSize),
      game.tileSize * btnRectSizeX,
      game.tileSize * btnRectSizeY,
    );
  }
}
