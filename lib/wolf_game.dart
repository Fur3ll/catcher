import 'dart:io';
import 'dart:ui';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:catch_the_egg/componets/base/base_component.dart';
import 'package:catch_the_egg/componets/base/common/game_state.dart';
import 'package:catch_the_egg/componets/base/common/game_type.dart';
import 'package:catch_the_egg/componets/static/producers.dart';
import 'package:catch_the_egg/componets/text/text_flashing_display.dart';
import 'package:catch_the_egg/componets/wolf.dart';
import 'package:catch_the_egg/controllers/period.dart';
import 'package:flame/game.dart';
import 'package:flame/sprite.dart';
import 'package:flame/svg.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'componets/base/common/orientations.dart';
import 'componets/base/tapable.dart';
import 'componets/basket.dart';
import 'componets/buttons.dart';
import 'componets/static/background.dart';
import 'componets/static/backyard.dart';
import 'componets/static/customers.dart';
import 'componets/text/text_display.dart';
import 'controllers/fallen.dart';
import 'controllers/farm.dart';
import 'controllers/penalty.dart';
import 'controllers/spawner.dart';

typedef void DroppedCallback(Orient orient);
typedef void FinishedAnimCallback();
typedef void CatchCallback();

/// At first, the eggs roll slowly, but gradually the pace of the game
/// accelerates. After scoring every whole hundred points, the pace of
/// the game slows down a bit.
///
/// In the event of an egg drop, a penalty point is added to the player,
/// which is indicated by the image of the chicken. If the fall occurred
/// in the presence of the rabbit, leaning out of the window of the house
/// in the left corner, then the player is added half the penalty point,
/// the image of the chicken flashes. When 200 and 500 eggs are picked in
/// a basket, the penalty points will be void. After receiving three
/// penalty points (from three to six egg drops), the game stops.
///
/// Upon reaching 999 points, the game continues from the score 0,
/// the penalty collected is not reset (in the future it can be reset
/// when 200 points are reached again).
///
/// The game has two degrees of difficulty: gameA and gameB.
/// Game A means that the eggs begin to roll only in three
/// trays at a time, in Game B - in all four. The unused tray in Game
/// A depends on the number of penalty points: at 0 points, the lower
/// left tray is not used; at 0.5 and 1 penalty points, the lower right
/// tray; at 1.5 and 2 penalty points, the upper left tray; and
/// at 2.5 penalty points - upper right tray. When canceling penalties,
/// an unused tray changes to the lower left, as with 0 penalties,
/// as soon as all eggs roll on it.
///
/// In original handheld system was clock mode. When the alarm sounds,
/// rabbit pops out and rings the bell.
class WolfGame extends Game {
  final SharedPreferences storage;
  final Sprite background = Sprite('bg/back.png');
  final Svg backyard = Svg('images/bg/bg.svg');
  final String pause = 'пауза';
  final String gameA = 'игра А';
  final String gameB = 'игра Б';
  final String emptyString = '';
  final String win = 'вы выиграли';
  final String loos = 'вы проиграли';
  final String platformPrefixIos = 'audio/ios/';
  final String platformPrefixAndroid = 'audio/android/';

  final String soundFarm1 = 'farms/farm_1';
  final String soundFarm2 = 'farms/farm_2';
  final String soundFarm3 = 'farms/farm_3';
  final String soundFarm4 = 'farms/farm_4';
  final String soundCatch = 'catch';
  final String soundFall = 'fall';
  final String platformFormatIos = '.mp3';
  final String platformFormatAndroid = '.wav';

  final int tileProportionsNormal = 9;
  final int tileProportionsWide = 10;
  final double centerDx = .52;
  final double centerDy = -1.2;
  final double scoreDisplayDx = 1.5;
  final double scoreDisplayScale = .375;
  final double displayModeADx = 2.5;
  final double displayModeBDx = -1.4;
  final double penaltyDx = 1.3;
  final double penaltyDy = 1.25;
  final double displayModeScale = .2;
  final double minSpeed = .32;
  final double maxSpeed = .1;
  final double speedIndulge = .08;
  final double normalAspectRatio = 1.8;
  final int minEggs = 1;
  final int maxEggs = 8;
  final int minScore = 0;
  final int maxScore = 999;

  AudioCache audioFarm1;
  AudioCache audioFarm2;
  AudioCache audioFarm3;
  AudioCache audioFarm4;
  AudioCache audioCatchFall;

  String formatPrefix;
  GameType gameType = GameType.gameA;
  GameState gameState = GameState.start;
  Orient orient = Orient.leftTop;
  Size screenSize;
  double tileSize;
  double screenWightHalf;
  double screenHeightHalf;
  int score;
  int maxEggsOnScreen;
  double speed;

  List<BaseComponent> baseComponentsList;
  List<Button> btnList;
  List<TapAble> tapAbleList;
  List<Farm> farmList;
  List<Fallen> fallenList;

  TextDisplay scoreDisplay;
  FlashingText pauseDisplay;
  TextDisplay gameDisplayModeA;
  TextDisplay gameDisplayModeB;
  EggSpawner spawner;
  Penalty penalty;
  Period period;

  WolfGame(this.storage) {
    initialize();
    _initGame();
  }

  Future<void> initialize() async {
    btnList = List<Button>();
    tapAbleList = List<TapAble>();
    farmList = List<Farm>();
    fallenList = List<Fallen>();
    penalty = Penalty(this, penaltyDx, penaltyDy);
    baseComponentsList = List<BaseComponent>();

    resize(Size.zero);

    baseComponentsList.add(Background(this, background));
    baseComponentsList.add(Backyard(this, backyard));
    scoreDisplay =
        TextDisplay(this, centerDx, scoreDisplayDx, scoreDisplayScale);
    pauseDisplay = FlashingText(this, centerDx, centerDy, displayModeScale);
    gameDisplayModeA =
        TextDisplay(this, displayModeADx, centerDy, displayModeScale);
    gameDisplayModeB =
        TextDisplay(this, displayModeBDx, centerDy, displayModeScale);

    period = Period(this);
    farmList.add(Farm(this, Orient.leftTop));
    farmList.add(Farm(this, Orient.rightTop));
    farmList.add(Farm(this, Orient.leftBot));
    farmList.add(Farm(this, Orient.rightBot));
    fallenList.add(Fallen(this, DropOrient.leftDrop, _onFinished));
    fallenList.add(Fallen(this, DropOrient.rightDrop, _onFinished));
    spawner = EggSpawner(this, farmList, _onDropped, _onCatch);
    tapAbleList.add(Wolf(this, DropOrient.leftDrop));
    tapAbleList.add(Wolf(this, DropOrient.rightDrop));
    tapAbleList.add(Basket(this, Orient.rightBot));
    tapAbleList.add(Basket(this, Orient.rightTop));
    tapAbleList.add(Basket(this, Orient.leftBot));
    tapAbleList.add(Basket(this, Orient.leftTop));
    btnList.add(TopLeftButton(this));
    btnList.add(BotLeftButton(this));
    btnList.add(TopRightButton(this));
    btnList.add(BotRightButton(this));
    btnList.add(ModeButtonA(this));
    btnList.add(ModeButtonB(this));
    btnList.add(MenuButton(this));

    String prefix;
    if (Platform.isIOS) {
      prefix = platformPrefixIos;
      formatPrefix = platformFormatIos;
    } else {
      prefix = platformPrefixAndroid;
      formatPrefix = platformFormatAndroid;
    }

    audioFarm1 = AudioCache(
        fixedPlayer: AudioPlayer(), respectSilence: true, prefix: prefix);
    audioFarm2 = AudioCache(
        fixedPlayer: AudioPlayer(), respectSilence: true, prefix: prefix);
    audioFarm3 = AudioCache(
        fixedPlayer: AudioPlayer(), respectSilence: true, prefix: prefix);
    audioFarm4 = AudioCache(
        fixedPlayer: AudioPlayer(), respectSilence: true, prefix: prefix);
    audioCatchFall = AudioCache(
        fixedPlayer: AudioPlayer(), respectSilence: true, prefix: prefix);

    audioFarm1.load(soundFarm1 + formatPrefix);
    audioFarm2.load(soundFarm2 + formatPrefix);
    audioFarm3.load(soundFarm3 + formatPrefix);
    audioFarm4.load(soundFarm4 + formatPrefix);
    audioCatchFall.loadAll(<String>[
      soundCatch + formatPrefix,
      soundFall + formatPrefix,
    ]);

    audioFarm1.disableLog();
    audioFarm2.disableLog();
    audioFarm3.disableLog();
    audioFarm4.disableLog();
    audioCatchFall.disableLog();
  }

  void resize(Size size) {
    super.resize(size);
    screenSize = size;
    screenHeightHalf = screenSize.height / 2;
    screenWightHalf = screenSize.width / 2;

    int tileProportions;
    if (screenSize.width / screenSize.height > normalAspectRatio) {
      tileProportions = tileProportionsWide;
    } else {
      tileProportions = tileProportionsNormal;
    }

    tileSize = screenSize.width / tileProportions;

    btnList.forEach((Button btn) => btn?.resize());
    baseComponentsList?.forEach((BaseComponent btn) => btn?.resize());
    tapAbleList.forEach((TapAble itm) => itm?.resize());
    farmList.forEach((Farm itm) => itm?.resize());
    fallenList.forEach((Fallen itm) => itm?.resize());
    penalty?.resize();
    period?.resize();
    scoreDisplay?.resize(score.toString());

    switch (gameState) {
      case GameState.start:
        pauseDisplay?.resize(emptyString);
        break;
      case GameState.pause:
        pauseDisplay?.resize(pause);
        break;
      case GameState.finished:
        pauseDisplay?.resize(emptyString);
        break;
    }

    switch (gameType) {
      case GameType.gameA:
        gameDisplayModeA?.resize(gameA);
        gameDisplayModeB?.resize(emptyString);
        break;
      case GameType.gameB:
        gameDisplayModeA?.resize(emptyString);
        gameDisplayModeB?.resize(gameB);
        break;
    }
  }

  @override
  void render(Canvas canvas) {
    baseComponentsList?.forEach((BaseComponent btn) => btn?.render(canvas));
    tapAbleList?.forEach((TapAble itm) => itm?.render(canvas));
    scoreDisplay?.render(canvas);
    farmList?.forEach((Farm itm) => itm?.render(canvas));
    fallenList?.forEach((Fallen itm) => itm?.render(canvas));
    penalty?.render(canvas);
    period?.render(canvas);
    pauseDisplay?.render(canvas);
    gameDisplayModeA?.render(canvas);
    gameDisplayModeB?.render(canvas);
    btnList?.forEach((Button btn) => btn?.render(canvas));
  }

  @override
  void update(double t) {
    spawner?.update(t);
    penalty?.update(t);
    period?.update(t);
    fallenList?.forEach((Fallen itm) => itm?.update(t));
    pauseDisplay?.update(t);
  }

  void lifecycleStateChange(AppLifecycleState state) {
    if (state == AppLifecycleState.inactive) {
      _pauseGame();
    }
  }

  void onTapDown(TapDownDetails d) {
    bool isHandled = false;

    if (!isHandled) {
      btnList.forEach((Button button) {
        if (button.rect.contains(d.globalPosition)) {
          if (button is ControlButton) {
            if (button.orient == Orient.leftTop ||
                button.orient == Orient.leftBot) {
              if (button.orient == Orient.leftBot) {
                orient = Orient.leftBot;
              } else {
                orient = Orient.leftTop;
              }
              tapAbleList.forEach((TapAble itm) => itm?.onTapDown());
              isHandled = true;
            } else {
              if (button.orient == Orient.rightBot) {
                orient = Orient.rightBot;
              } else {
                orient = Orient.rightTop;
              }
              tapAbleList.forEach((TapAble itm) => itm?.onTapDown());
              isHandled = true;
            }
          } else if (button is ModeButton) {
            if (button.runtimeType == MenuButton) {
              if (gameState == GameState.pause) {
                _startGame();
                isHandled = true;
              } else if (gameState == GameState.finished) {
                isHandled = true;
              } else {
                _pauseGame();
                isHandled = true;
              }
            } else if (button.runtimeType == ModeButtonA) {
              gameType = GameType.gameA;
              _restartGame();
              isHandled = true;
            } else if (button.runtimeType == ModeButtonB) {
              gameType = GameType.gameB;
              _restartGame();
              isHandled = true;
            }
          }
        }
      });
    }
  }

  void _onDropped(Orient orient) {
    spawner.stop();
    period.pause();
    DropOrient dropOrient = DropOrient.rightDrop;
    if (orient == Orient.leftTop || orient == Orient.leftBot) {
      dropOrient = DropOrient.leftDrop;
    }
    fallenList.forEach((Fallen itm) {
      if (itm.orient == dropOrient) {
        itm.start();
      }
    });
  }

  void _onFinished() {
    if (period.isPeriod) {
      penalty.setHalfPenalty();
    } else {
      penalty.setPenalty();
    }
    if (penalty.penalty < penalty.maxPenalty) {
      spawner.clear();
      _setDifficultySpawnerLevel(penalty.penalty);
      spawner.start();
      period.restart();
    } else {
      gameState = GameState.finished;
      spawner.clear();
      period.stop();
      pauseDisplay.updateText(loos);
      pauseDisplay.setVisible();
    }
  }

  void _displayGameType() {
    switch (gameType) {
      case GameType.gameA:
        gameDisplayModeA.updateText(gameA);
        gameDisplayModeA.setVisible();
        gameDisplayModeB.setGone();
        break;
      case GameType.gameB:
        gameDisplayModeB.updateText(gameB);
        gameDisplayModeB.setVisible();
        gameDisplayModeA.setGone();
        break;
    }
  }

  void _onCatch() {
    score += 1;
    _updateDifficulty();
    _score2speed(score);
    scoreDisplay.updateText(score.toString());
    if (score >= 1000) {
      _stopGame();
    }
  }

  void _updateDifficulty() {
    switch (score) {
      case 0:
        maxEggsOnScreen = 1;
        break;
      case 5:
        maxEggsOnScreen = 2;
        break;
      case 10:
        maxEggsOnScreen = 3;
        break;
      case 50:
        maxEggsOnScreen = 4;
        break;
      case 100:
        maxEggsOnScreen = 5;
        break;
      case 200:
        penalty.reset();
        _setDifficultySpawnerLevel(penalty.penalty);
        break;
      case 500:
        maxEggsOnScreen = 6;
        penalty.reset();
        _setDifficultySpawnerLevel(penalty.penalty);
        break;
      case 700:
        maxEggsOnScreen = 7;
        break;
      case 900:
        maxEggsOnScreen = 8;
        break;
    }
  }

  void _setDifficultySpawnerLevel(int penalty) {
    if (gameType == GameType.gameA) {
      spawner.difficultyChange(penalty);
    } else {
      spawner.difficultyChange(spawner.defaultDifficulty);
    }
  }

  void _initGame() {
    score = minScore;
    speed = minSpeed;
    _updateDifficulty();
    _setDifficultySpawnerLevel(penalty.penalty);
  }

  void _restartGame() {
    _initGame();
    penalty.reset();
    gameState = GameState.start;
    spawner.clear();
    spawner.start();
    fallenList.forEach((Fallen itm) => itm.reset());
    period?.start();
    scoreDisplay?.updateText(score.toString());
    pauseDisplay?.setGone();
    _displayGameType();
  }

  void _stopGame() {
    gameState = GameState.finished;
    pauseDisplay.updateText(win);
    pauseDisplay.setVisible();
    period.stop();
    spawner.clear();
    spawner.stop();
  }

  void _pauseGame() {
    if (gameState != GameState.finished) {
      gameState = GameState.pause;
      pauseDisplay.updateText(pause);
      pauseDisplay.setVisible();
      period.pause();
      spawner.stop();
      fallenList.forEach((Fallen itm) => itm.pause());
    }
  }

  void _startGame() {
    gameState = GameState.start;
    pauseDisplay.setGone();
    bool isFallen = false;
    fallenList.forEach((Fallen itm) {
      if (itm.active) {
        itm.start();
        isFallen = true;
      }
    });

    if (!isFallen) {
      period.restart();
      spawner.start();
    }
  }

  void _score2speed(int score) {
    speed = (score - minScore) / (maxScore - minScore) * (maxSpeed - minSpeed) +
        minSpeed;

    if (gameType == GameType.gameA) {
      speed += speedIndulge;
    }
  }
}
